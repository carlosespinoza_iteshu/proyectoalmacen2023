﻿using BIZ;
using COMMON.Entidades;

namespace MovilApp
{
    public partial class MainPage : ContentPage
    {

        int cantidad = 1;
        MovimientoManager manager;
        public MainPage()
        {
            InitializeComponent();
            pkrProducto.ItemsSource = FabricManager.ProductoManager().ObtenerTodos;
            pkrAlmacenista.ItemsSource = FabricManager.AlmacenistaManager().ObtenerTodos;
            var almacenes = FabricManager.AlmacenesManager().ObtenerTodos;
            pkrAlmacenOrigen.ItemsSource = almacenes;
            pkrAlmacenDestino.ItemsSource = almacenes;
            entCantidad.Text = cantidad.ToString();
            manager = FabricManager.MovimientoManager();
        }

        private void btnMenos_Clicked(object sender, EventArgs e)
        {

            if (cantidad > 1)
            {
                cantidad--;
            }
            entCantidad.Text = cantidad.ToString();
        }

        private void btnMas_Clicked(object sender, EventArgs e)
        {
            cantidad++;
            entCantidad.Text = cantidad.ToString();
        }

        private void btnGuardar_Clicked(object sender, EventArgs e)
        {
            bool bandera = false;
            Movimiento mov = new Movimiento();
            mov.FechaHora = DateTime.Now;
            Producto producto = pkrProducto.SelectedItem as Producto;
            if (producto == null)
            {
                DisplayAlert("Error", "Selecciona un producto", "Ok");
                return;
            }
            mov.IdProducto = producto.Id;
            Almacenista almacenista = pkrAlmacenista.SelectedItem as Almacenista;
            if (almacenista == null)
            {
                DisplayAlert("Error", "Selecciona un almacenista", "Ok");
                return;
            }
            mov.IdAlmacenista = almacenista.Id;
            Almacen origen = pkrAlmacenOrigen.SelectedItem as Almacen;
            Almacen destino = pkrAlmacenDestino.SelectedItem as Almacen;
            if (origen == destino)
            {
                DisplayAlert("Error", "El destino y el origen no puede ser el mismo", "Error");
                return;
            }
            if (origen == null && destino != null)
            {
            //entrada
                mov.IdAlmacenDestino = destino.Id;
                mov.IdAlmacenOrigen = null;
                mov.Motivo = "Entrada";
            }
            else
            {

                if (origen != null && destino == null)
                {
                    //salida
                    mov.IdAlmacenOrigen = origen.Id;
                    mov.IdAlmacenDestino = null;
                    mov.Motivo = "Salida";
                }
                else
                {
                    //transferencia
                    mov.IdAlmacenOrigen = origen.Id;
                    mov.IdAlmacenDestino = destino.Id;
                    mov.Motivo = "Transferencia";
                }
            }
            mov.Notas = entCantidad.Text;
            if (manager.Insertar(mov) != null)
            {
                DisplayAlert("Almacen", "Guardado correctamente", "Ok");
                //Limpiar
            }
            else
            {
                DisplayAlert("Error", manager.Error, "Ok");
            }
        }

        private void entCantidad_TextChanged(object sender, TextChangedEventArgs e)
        {
            cantidad = int.Parse(e.NewTextValue);
        }
    }
}