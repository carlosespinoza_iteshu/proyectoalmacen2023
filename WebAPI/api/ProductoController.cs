﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoController : GenericController<Producto>
    {
        public ProductoController():base(FabricRepository.ProductoRepositorio())
        {
            
        }
    }
}
