﻿using COMMON.Entidades;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductoEnAlmacenController : GenericController<ProductoEnAlmacen>
    {
        public ProductoEnAlmacenController():base(FabricRepository.ProductoEnAlmacenRepositorio())
        {
            
        }
    }
}
