﻿using COMMON.Entidades;
using COMMON.Interfaces;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class AlmacenController : GenericController<Almacen>
    {
        public AlmacenController() : base(FabricRepository.AlmacenRepositorio())
        {
        }
    }
}
