﻿using COMMON.Entidades;
using COMMON.Modelos;
using DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class MovimientoController : GenericController<Movimiento>
    {
        public MovimientoController():base(FabricRepository.MovimientoRepositorio())
        {
            
        }
        [HttpGet("ObtenerReporte")]
        public ActionResult< List<ReporteMovimientosModel>> ObtenerReporteDeMovimientos()
        {
            try
            {
                return Ok(repositorio.Query<ReporteMovimientosModel>("Select * from reporte"));
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
           
        }
    }
}
