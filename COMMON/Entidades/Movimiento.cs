﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Movimiento:Base
    {
        public int IdProducto { get; set; }
        public int? IdAlmacenOrigen { get; set; }
        public int? IdAlmacenDestino { get; set; }
        public int IdAlmacenista{ get; set; }
        public string Motivo { get; set; }
        public string? Notas { get; set; }
    }
}
