﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class ProductoEnAlmacen:Base
    {
        public int IdProducto { get; set; }

        public int IdAlmacen { get; set; }
        public int Cantidad { get; set; }
    }
}
