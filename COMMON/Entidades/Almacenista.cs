﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Entidades
{
    public class Almacenista:Base
    {
        public string Nombres { get; set; }
        public string Apellidos { get; set; }

        public string? UrlImagen { get; set; }
        public string Password { get; set; }
        public string? Notas { get; set; }

        public override string ToString()
        {
            return Nombres + " " + Apellidos;
        }
    }
}
