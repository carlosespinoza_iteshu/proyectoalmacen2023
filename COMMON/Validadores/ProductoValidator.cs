﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class ProductoValidator:BaseValidator<Producto>
    {
        public ProductoValidator()
        {
            RuleFor(p => p.Nombre).NotEmpty().MaximumLength(100);
            RuleFor(p => p.UrlImagen).MaximumLength(500);
        }
    }
}
