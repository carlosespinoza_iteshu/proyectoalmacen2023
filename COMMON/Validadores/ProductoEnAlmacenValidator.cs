﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class ProductoEnAlmacenValidator:BaseValidator<ProductoEnAlmacen>
    {
        public ProductoEnAlmacenValidator()
        {
            RuleFor(p => p.IdProducto).GreaterThan(0);
            RuleFor(p => p.IdAlmacen).GreaterThan(0);
            RuleFor(p => p.Cantidad).GreaterThanOrEqualTo(0);
        }
    }
}
