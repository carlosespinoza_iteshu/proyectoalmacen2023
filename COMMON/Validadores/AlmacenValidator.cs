﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class AlmacenValidator:BaseValidator<Almacen>
    {
        public AlmacenValidator()
        {
            RuleFor(a => a.Nombre).NotEmpty().MaximumLength(100);
            RuleFor(a => a.Ubicacion).NotEmpty().MaximumLength(500);
        }
    }
}
