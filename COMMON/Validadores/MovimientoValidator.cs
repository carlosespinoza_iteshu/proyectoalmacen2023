﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class MovimientoValidator:BaseValidator<Movimiento>
    {
        public MovimientoValidator()
        {
            RuleFor(m => m.IdAlmacenista).NotEmpty();
            RuleFor(m => m.Motivo).NotEmpty().MaximumLength(500);
            RuleFor(m => m.IdProducto).NotEmpty().GreaterThan(0);
            RuleFor(m => m.IdAlmacenOrigen).GreaterThan(0);
            RuleFor(m => m.IdAlmacenDestino).GreaterThan(0);
            RuleFor(m => m.Notas).MaximumLength(500);

        }
    }
}
