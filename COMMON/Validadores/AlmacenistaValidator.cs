﻿using COMMON.Entidades;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Validadores
{
    public class AlmacenistaValidator:BaseValidator<Almacenista>
    {
        public AlmacenistaValidator()
        {
            RuleFor(a => a.Nombres).NotEmpty().MaximumLength(100);
            RuleFor(a => a.Apellidos).NotEmpty().MaximumLength(100);
            RuleFor(a => a.UrlImagen).MaximumLength(500);
            RuleFor(a => a.Password).NotEmpty().MaximumLength(50);
            RuleFor(a => a.Notas).MaximumLength(500);
        }
    }
}
