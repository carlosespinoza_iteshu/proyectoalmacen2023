﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Modelos
{
    public class ReporteMovimientosModel:ModelBase
    {
        public int Id { get; set; }
        public DateTime FechaHora { get; set; }
        public int IdProducto { get; set; }
        public string UrlProducto { get; set; }
        public string NombreProducto { get; set; }
        public int? IdAlmacenOrigen { get; set; }
        public string? NombreAlmacenOrigen { get; set; }
        public int? IdAlmacenDestino { get; set; }
        public string? NombreAlmacenDestino { get; set; }
        public int IdAlmacenista { get; set; }
        public string NombreAlmacenista { get; set; }
        public string Motivo { get; set; }
        public string Cantidad { get; set; }

    }
}
