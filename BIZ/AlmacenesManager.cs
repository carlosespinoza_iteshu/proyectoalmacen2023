﻿using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class AlmacenesManager : GenericManager<Almacen>
    {
        public AlmacenesManager(string urlBase, BaseValidator<Almacen> validador) : base(urlBase, validador)
        {
        }
    }
}
