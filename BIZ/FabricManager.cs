﻿using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public static class FabricManager
    {
        public static string urlBase = @"http://localhost:5098";

        public static ProductoManager ProductoManager()
        {
            return new ProductoManager(urlBase, new ProductoValidator());
        }

        public static AlmacenesManager AlmacenesManager()
        {
            return new AlmacenesManager(urlBase, new AlmacenValidator());
        }

        public static AlmacenistaManager AlmacenistaManager()
        {
            return new AlmacenistaManager(urlBase, new AlmacenistaValidator());
        }

        public static MovimientoManager MovimientoManager()
        {
            return new MovimientoManager(urlBase, new MovimientoValidator());
        }





    }
}
