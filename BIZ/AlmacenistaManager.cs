﻿using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class AlmacenistaManager : GenericManager<Almacenista>
    {
        public AlmacenistaManager(string urlBase, BaseValidator<Almacenista> validador) : base(urlBase, validador)
        {
        }
    }
}
