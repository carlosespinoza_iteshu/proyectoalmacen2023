﻿using COMMON.Entidades;
using COMMON.Modelos;
using COMMON.Validadores;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class MovimientoManager : GenericManager<Movimiento>
    {
        public MovimientoManager(string urlBase, BaseValidator<Movimiento> validador) : base(urlBase, validador)
        {
        }

        public List<ReporteMovimientosModel> ObtenerReporteDeMovimientos()
        {
            return ObtenerReporteDeMovimientosAsync().Result;
        }


        private async Task<List<ReporteMovimientosModel>> ObtenerReporteDeMovimientosAsync()
        {
            HttpResponseMessage response = await httpClient.GetAsync($"{urlBase}/api/{tabla}/ObtenerReporte").ConfigureAwait(false);
            var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
            if (response.IsSuccessStatusCode)
            {
                Error = "";
                return JsonConvert.DeserializeObject<List<ReporteMovimientosModel>>(content);
            }
            else
            {
                Error = content;
                return null;
            }
        }
    }
}
