﻿using COMMON.Entidades;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BIZ
{
    public class ProductoManager : GenericManager<Producto>
    {
        public ProductoManager(string urlBase, BaseValidator<Producto> validador) : base(urlBase, validador)
        {
        }
    }
}
