﻿using COMMON.Entidades;
using COMMON.Interfaces;
using COMMON.Validadores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class FabricRepository
    {
        //Importante agregar el TrustServerCertificate=true a la cadena de conexión
        //Cambiar cadena de conexión por la propia
        private static string cadenaConexion = @"workstation id=almacenprofecarlos.mssql.somee.com;packet size=4096;user id=profeCarlos_SQLLogin_4;pwd=jzivfyris6;data source=almacenprofecarlos.mssql.somee.com;persist security info=False;initial catalog=almacenprofecarlos;TrustServerCertificate=True;";

        public static IRepositorio<Almacen> AlmacenRepositorio()
        {
            return new Repositorio<Almacen>(cadenaConexion, new AlmacenValidator());
        }

        public static IRepositorio<Almacenista> AlmacenistaRepositorio()
        {
            return new Repositorio<Almacenista>(cadenaConexion, new AlmacenistaValidator());
        }

        public static IRepositorio<Movimiento> MovimientoRepositorio() => new Repositorio<Movimiento>(cadenaConexion, new MovimientoValidator());
        
        
        public static IRepositorio<Producto> ProductoRepositorio() => new Repositorio<Producto>(cadenaConexion, new ProductoValidator());

        public static IRepositorio<ProductoEnAlmacen> ProductoEnAlmacenRepositorio() => new Repositorio<ProductoEnAlmacen>(cadenaConexion, new ProductoEnAlmacenValidator());
        
    }
}
